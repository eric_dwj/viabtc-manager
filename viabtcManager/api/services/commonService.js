
const jsonfile = require('jsonfile');
var      shell = require('shelljs');


module.exports = {


       exeCommand:function(command){
        return shell.exec(command);

       }, 
       readJson: function(file) {
        return new Promise((resolve, reject) => {
        jsonfile.readFile(file, function (err, obj) {
            if (err)
            {
              reject(err);
            } 
            else
            {
              resolve(obj);
              
            }  
            
        });
      });
    },

    writeJson: function(file,obj) {
      return new Promise((resolve, reject) => {
        jsonfile.writeFile(file, obj, function (err) {
     console.log("obj",obj);

         if (err) 
          {
            reject(err);
          }
         else
         {
            resolve("success");
         }

        });
      });
    }

}