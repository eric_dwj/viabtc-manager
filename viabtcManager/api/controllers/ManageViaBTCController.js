/**
 * ConfigController
 *
 * @description :: Server-side logic for managing configs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

        restartServer:function(req,res){
          var ret ={};
          var restartMatchEngine ="./path/restart.sh";
          var restartMarketPrice ="./path/restart.sh";
          ret.matchengine = commonService.exeCommand(restartMatchEngine);
          ret.marketprice = commonService.exeCommand(restartMarketPrice);
          res.json(ret);

        },
	getConfig : function (req, res) {

        var filetype = req.query.filetype;

        if( filetype && filetype =="matchengine" )
        {
        	commonService
        	.readJson(sails.config.globals.matchengineConfig)
        	.then(( matchengineRes )=>{
        		res.json( matchengineRes );
        	});
        }  
	},
	updateConfig : function (req, res) {

	var configJson = req.body.configjson;
		
                commonService
                .writeJson(sails.config.globals.matchengineConfig,configJson)
                .then(( result )=>{
                        res.json( result );
                });
	}
	
};

